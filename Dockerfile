FROM node:10-alpine as builder
COPY package.json yarn.lock ./
RUN yarn install && mkdir /docker-task && mv ./node_modules ./docker-task
WORKDIR /docker-task
COPY . .
RUN yarn run build

FROM nginx:alpine
#!/bin/sh
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /docker-task/build /usr/share/nginx/html
EXPOSE 3000 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]