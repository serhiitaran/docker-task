import {
  SET_CHAT_DATA,
  TOGGLE_MESSAGE_LIKE,
  SHOW_EDIT_MESSAGE_POPUP,
  HIDE_EDIT_MESSAGE_POPUP,
  EDIT_MESSAGE,
  ADD_MESSAGE,
  DELETE_MESSAGE
} from '../actions/actionTypes';

import {
  setChatData,
  toggleMessageLike,
  showEditMessagePopup,
  hideEditMessagePopup,
  editMessage,
  addMessage,
  deleteMessage
} from '../helpers/reducerHelper';

const initialState = {
  isLoading: true,
  chatName: '',
  messages: [],
  user: {},
  editMessagePopup: { isEdit: false, id: '', text: '' }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CHAT_DATA:
      return setChatData(state, action.payload);
    case TOGGLE_MESSAGE_LIKE:
      return toggleMessageLike(state, action.payload);
    case SHOW_EDIT_MESSAGE_POPUP:
      return showEditMessagePopup(state, action.payload);
    case HIDE_EDIT_MESSAGE_POPUP:
      return hideEditMessagePopup(state);
    case EDIT_MESSAGE:
      return editMessage(state, action.payload);
    case ADD_MESSAGE:
      return addMessage(state, action.payload);
    case DELETE_MESSAGE:
      return deleteMessage(state, action.payload);
    default:
      return state;
  }
};

export default reducer;
