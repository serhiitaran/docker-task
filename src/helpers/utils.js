export const getDate = gettingDate => {
  const formatedDate = gettingDate.toString();
  const date = formatedDate.substring(0, 10);
  const time = formatedDate.substring(15, 21);
  return [date, time];
};

export const formatDate = isoDate => {
  return new Date(isoDate);
};

export const getIndex = (items, id) => {
  return items.findIndex(item => item.id === id);
};

export const idGenerator = () => {
  const idPart = () => {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  };
  return idPart() + idPart() + '-' + idPart() + '-' + idPart() + '-' + idPart() + '-' + idPart() + idPart() + idPart();
};
