import { getIndex, idGenerator, getDate } from './utils';

export const setChatData = (state, actionPayload) => {
  const { chatName, messages, user } = actionPayload;
  const updatedState = {
    ...state,
    chatName,
    messages,
    user,
    isLoading: false
  };
  return updatedState;
};

export const toggleMessageLike = (state, actionPayload) => {
  const { id } = actionPayload;
  const messageIndex = getIndex(state.messages, id);
  const oldMessage = state.messages[messageIndex];
  const newLikeValue = !oldMessage.isLike;
  const updatedMessage = { ...oldMessage, isLike: newLikeValue };
  const updatedMessages = [
    ...state.messages.slice(0, messageIndex),
    updatedMessage,
    ...state.messages.slice(messageIndex + 1)
  ];
  const updatedState = {
    ...state,
    messages: updatedMessages
  };
  return updatedState;
};

export const showEditMessagePopup = (state, actionPayload) => {
  const { id, text } = actionPayload;
  const updatedState = {
    ...state,
    editMessagePopup: { isEdit: true, id, text }
  };
  return updatedState;
};

export const hideEditMessagePopup = state => {
  const updatedState = {
    ...state,
    editMessagePopup: { isEdit: false, id: '', text: '' }
  };
  return updatedState;
};

export const editMessage = (state, actionPayload) => {
  const { id, text } = actionPayload;
  const messageIndex = getIndex(state.messages, id);
  const oldMessage = state.messages[messageIndex];
  const updatedMessage = { ...oldMessage, text, isEdited: true };
  const updatedMessages = [
    ...state.messages.slice(0, messageIndex),
    updatedMessage,
    ...state.messages.slice(messageIndex + 1)
  ];
  const updatedState = {
    ...state,
    messages: updatedMessages
  };
  return updatedState;
};

export const addMessage = (state, actionPayload) => {
  const { text } = actionPayload;
  const messageId = idGenerator();
  const [date, time] = getDate(new Date());
  const user = { ...state.user };
  const newMessage = {
    id: messageId,
    text,
    date,
    time,
    isLike: false,
    isEdited: false,
    ...user
  };
  const updatedMessages = [...state.messages, newMessage];
  const updatedState = {
    ...state,
    messages: updatedMessages
  };
  return updatedState;
};

export const deleteMessage = (state, actionPayload) => {
  const { id } = actionPayload;
  const messageIndex = getIndex(state.messages, id);
  const updatedMessages = [...state.messages.slice(0, messageIndex), ...state.messages.slice(messageIndex + 1)];
  const updatedState = {
    ...state,
    messages: updatedMessages
  };
  return updatedState;
};
