import { formatDate, getDate } from './utils';

export const getUsersCount = messages => {
  return messages.length && new Set(messages.map(message => message.user)).size;
};

export const getMessagesCount = messages => messages.length;

export const getLastMessageTime = messages => messages.length && messages.slice(-1)[0].time;

export const createMessages = data => {
  if (!data) {
    return [];
  }
  const sortedMessages = data
    .map(message => {
      const createdAt = formatDate(message.createdAt);
      return { ...message, createdAt };
    })
    .sort((a, b) => a.createdAt - b.createdAt);
  const messages = sortedMessages.map(message => {
    const { id, text, userId, user, avatar, createdAt, editedAt } = message;
    const [date, time] = getDate(createdAt);
    const isEdited = editedAt !== '';
    return { id, text, userId, user, avatar, date, time, isLike: false, isEdited };
  });
  return messages;
};

export const getUserLastMessage = (messages, userId) => {
  const allUserMessages = messages.filter(message => message.userId === userId);
  if (allUserMessages.length) {
    const { id, text } = allUserMessages.slice(-1)[0];
    return { isMessageExist: true, id, text };
  }
  return { isMessageExist: false, id: '', text: '' };
};
