import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './messageInput.css';

export class MessageInput extends Component {
  state = {
    value: ''
  };

  handleChange = ({ target }) => {
    const value = target.value;
    this.setState({ value });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { value } = this.state;
    if (!value) {
      return;
    }
    const { onMessageAdd } = this.props;
    onMessageAdd(value);
    this.setState({
      value: ''
    });
  };

  render() {
    return (
      <div className="message-input">
        <form onSubmit={this.handleSubmit}>
          <textarea
            placeholder="Type your message here..."
            onChange={this.handleChange}
            value={this.state.value}
          ></textarea>
          <div className="message-input__buttons">
            <button type="submit">Send</button>
          </div>
        </form>
      </div>
    );
  }
}

MessageInput.propTypes = {
  onMessageAdd: PropTypes.func.isRequired
};
