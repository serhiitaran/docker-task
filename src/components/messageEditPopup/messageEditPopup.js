import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './messageEditPopup.css';

export class MessageEditPopup extends Component {
  state = {
    value: ''
  };

  componentDidUpdate(prevProps) {
    if (prevProps.isEdit !== this.props.isEdit) {
      const { text } = this.props;
      this.setState({
        value: text
      });
    }
  }

  handleChange = ({ target }) => {
    const value = target.value;
    this.setState({ value });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { value } = this.state;
    const { hidePopup, editMessage, id } = this.props;

    const formatedValue = value.trim();
    if (!formatedValue) {
      return;
    } else {
      editMessage({ text: value, id });
      hidePopup();
    }
  };

  render() {
    const { isEdit, hidePopup } = this.props;
    const modalClassNames = isEdit ? 'modal-container modal-container--show' : 'modal-container';
    return (
      <div className={modalClassNames}>
        <div className="modal-wrapper">
          <div className="modal">
            <h3 className="modal__header">Edit message</h3>
            <form onSubmit={this.handleSubmit}>
              <div className="modal__content">
                <textarea
                  placeholder="Type your edited message here..."
                  onChange={this.handleChange}
                  value={this.state.value}
                ></textarea>
              </div>
              <div className="modal__buttons">
                <button className="modal__button" type="submit">
                  Update
                </button>
                <button className="modal__button modal__button--cancel" onClick={hidePopup}>
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

MessageEditPopup.propTypes = {
  isEdit: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  hidePopup: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired
};
