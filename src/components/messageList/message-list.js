import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Message from '../message';
import './message-list.css';

export class MessageList extends Component {
  messagesEndRef = React.createRef();

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate(prevProps) {
    const { messages: currentMessages } = this.props;
    const { messages } = prevProps;
    if (currentMessages.length > messages.length) {
      this.scrollToBottom();
    }
  }

  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  render() {
    const { currentUserId, messages, onMessageLike, onShowEditMessagePopup, onMessageDelete } = this.props;
    let messagesDate;
    const messageItems = messages.map(message => {
      const { id, date, ...messageProps } = message;
      const isSameDate = messagesDate === date;
      if (!isSameDate) {
        messagesDate = date;
      }
      const messageDateLine = (
        <div key={date} className="messages__date-line">
          <span>{date}</span>
        </div>
      );
      return (
        <React.Fragment key={id}>
          {!isSameDate && messageDateLine}
          <Message
            currentUserId={currentUserId}
            {...messageProps}
            onLike={() => onMessageLike(id)}
            onDelete={() => onMessageDelete(id)}
            onShowEditPopup={() => onShowEditMessagePopup({ id, text: messageProps.text })}
          />
        </React.Fragment>
      );
    });
    return (
      <div className="messages">
        {messageItems}
        <div ref={this.messagesEndRef}></div>
      </div>
    );
  }
}

MessageList.propTypes = {
  currentUserId: PropTypes.string.isRequired,
  messages: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
      user: PropTypes.string.isRequired,
      avatar: PropTypes.string.isRequired,
      time: PropTypes.string.isRequired,
      isLike: PropTypes.bool.isRequired,
      isEdited: PropTypes.bool.isRequired
    })
  ),
  onMessageLike: PropTypes.func.isRequired,
  onShowEditMessagePopup: PropTypes.func.isRequired,
  onMessageDelete: PropTypes.func.isRequired
};
