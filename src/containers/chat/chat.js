import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as actions from '../../actions';

import Loader from '../../components/loader';
import Header from '../../components/header';
import MessageList from '../../components/messageList';
import MessageInput from '../../components/messageInput';
import MessageEditPopup from '../../components/messageEditPopup';

import { getUsersCount, getMessagesCount, getLastMessageTime, getUserLastMessage } from '../../helpers/chatHelper';

import './chat.css';

class Chat extends Component {
  componentDidMount() {
    this.props.loadChatData();
    document.addEventListener('keydown', this.handleEditLastMessage);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleEditLastMessage);
  }

  handleEditLastMessage = event => {
    if (event.keyCode === 38) {
      const {
        state: { messages, user },
        showEditMessagePopup
      } = this.props;
      const { isMessageExist, id, text } = getUserLastMessage(messages, user.userId);
      if (isMessageExist) {
        showEditMessagePopup({ id, text });
      }
      return;
    }
  };

  render() {
    const {
      state: { isLoading, chatName, messages, user, editMessagePopup },
      toggleMessageLike,
      showEditMessagePopup,
      hideEditMessagePopup,
      editMessage,
      addMessage,
      deleteMessage
    } = this.props;

    const usersCount = getUsersCount(messages);
    const messagesCount = getMessagesCount(messages);
    const lastMessageTime = getLastMessageTime(messages);

    return isLoading ? (
      <Loader />
    ) : (
      <div className="chat">
        <Header
          chatName={chatName}
          usersCount={usersCount}
          messagesCount={messagesCount}
          lastMessageTime={lastMessageTime}
        />
        <MessageList
          currentUserId={user.userId}
          messages={messages}
          onMessageLike={toggleMessageLike}
          onMessageDelete={deleteMessage}
          onShowEditMessagePopup={showEditMessagePopup}
        />
        <MessageInput onMessageAdd={addMessage} />
        <MessageEditPopup {...editMessagePopup} hidePopup={hideEditMessagePopup} editMessage={editMessage} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    state
  };
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);

Chat.propTypes = {
  state: PropTypes.shape({
    chatName: PropTypes.string.isRequired,
    user: PropTypes.objectOf(PropTypes.string).isRequired,
    messages: PropTypes.arrayOf(
      PropTypes.shape({
        text: PropTypes.string.isRequired,
        userId: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired,
        avatar: PropTypes.string.isRequired,
        time: PropTypes.string.isRequired,
        isLike: PropTypes.bool.isRequired,
        isEdited: PropTypes.bool.isRequired
      })
    )
  }).isRequired,
  showEditMessagePopup: PropTypes.func.isRequired,
  hideEditMessagePopup: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired,
  addMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired
};
